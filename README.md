# Mock delivery system



## Setup environment variables

Please, setup all required environment variables before starting a server. Check `.env` file at the `root` directory.

```dotenv
# port number for server
PORT=9000

# database credentials
DB_USER=postgres
DB_PASSWORD=password
DB_NAME=postgres
DB_HOST=localhost
DB_PORT=5432

# google maps API key
GOOGLE_MAPS_API_KEY=test-api-key
```

IMPORTANT
- [ ] Set an available port number for Node.js server
- [ ] Set an available port number for Postgres database
- [ ] Set a valid API key for Google Maps APIs

## Start a server

To start the server manually:
```shell
yarn install
yarn build

docker-compose up -d db

yarn start
```

Or simply execute `start.sh` bash file:
```shell
chmod u+x start.sh
./start.sh
```

DO NOT forget to stop the `server`

## Testing

Set the API key for Google Maps APIs at `.jest/setEnvVars.js`

```typescript
process.env.GOOGLE_MAPS_API_KEY = "set-your-api-key-here";
```

To run the test cases:
```shell
yarn install
yarn build
yarn test
```