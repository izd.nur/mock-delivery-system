import { Router, Request, Response } from "express";
import { OrderService } from "../services/order-service";
import { OrderResponseType } from "../util/types/types";
import { checkRequestBodyIsValid } from "../util/util";
import { OrderModel } from "../entities/order-entity";

export class OrderController {
  public router: Router;
  private orderService: OrderService;

  constructor() {
    this.orderService = new OrderService();
    this.router = Router();
    this.routes();
  }

  public createOrder = async (req: Request, res: Response) => {
    const { origin, destination } = req.body;

    const requestBodyValidityResult = checkRequestBodyIsValid(
      origin,
      destination
    );
    if (!requestBodyValidityResult.valid) {
      return res.status(400).send({ error: requestBodyValidityResult.error });
    }

    const createdOrderResponse: OrderResponseType =
      await this.orderService.createOrder(requestBodyValidityResult.result!);

    return res.status(200).send(createdOrderResponse);
  };

  public listOrders = async (req: Request, res: Response) => {
    const { page, limit } = req.query;
    let queryPage = 1;
    let queryLimit = 500;

    if (page !== undefined && page.toString().length) {
      if (isNaN(parseInt(page.toString())) || parseInt(page.toString()) < 1) {
        return res.status(400).send({ error: "Bad request: page is invalid!" });
      }
      queryPage = parseInt(page.toString());
    }

    if (limit !== undefined && limit.toString().length) {
      if (isNaN(parseInt(limit.toString())) || parseInt(limit.toString()) < 0) {
        return res.status(400).send({ error: "Bad request: page is invalid!" });
      }
      queryLimit = parseInt(limit.toString());
    }

    const ordersListResponse: OrderModel[] = await this.orderService.listOrders(
      queryPage,
      queryLimit
    );

    return res.status(200).send(ordersListResponse);
  };

  public updateOrder = async (req: Request, res: Response) => {
    const id = req.params.id;
    const { status } = req.body;
    if (!status || status.toString().trim() !== "TAKEN") {
      return res.status(400).send({ error: "Bad request: status is invalid!" });
    }

    await this.orderService
      .getOrder(id)
      .then(async (order) => {
        if (order.status !== "UNASSIGNED") {
          return res
            .status(409)
            .send({ error: "Conflict: order already taken!" });
        } else {
          await this.orderService.updateOrder(id).then(() => {
            return res.status(200).send({ status: "SUCCESS" });
          });
        }
      })
      .catch(() => {
        return res.status(404).send({ error: "Not found: order not found!" });
      });
  };

  public routes() {
    this.router.post("/", this.createOrder);
    this.router.get("/", this.listOrders);
    this.router.patch("/:id", this.updateOrder);
  }
}
