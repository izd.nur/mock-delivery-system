import { OrderRoutePoints } from "./types/types";
import {
  Client,
  TravelMode,
  UnitSystem,
} from "@googlemaps/google-maps-services-js";

export const getDistanceFromGoogleAPI = async (
  orderPoints: OrderRoutePoints
): Promise<number> => {
  const { GOOGLE_MAPS_API_KEY } = process.env;
  if (!GOOGLE_MAPS_API_KEY) {
    console.warn("Google Maps API Key not provided");
    return -1;
  }
  const googleMapsClient = new Client();
  return await googleMapsClient
    .distancematrix({
      params: {
        origins: [[orderPoints.originLatitude, orderPoints.originLongitude]],
        destinations: [
          [orderPoints.destinationLatitude, orderPoints.destinationLongitude],
        ],
        mode: TravelMode.driving,
        key: process.env.GOOGLE_MAPS_API_KEY || "",
        units: UnitSystem.metric,
      },
    })
    .then(
      (response) => response.data.rows[0]?.elements[0]?.distance?.value || -1
    );
};

export const checkRequestBodyIsValid = (
  origin: string[],
  destination: string[]
): { valid: boolean; error?: string; result?: OrderRoutePoints } => {
  // check if origin and destination provided
  if (!origin || !destination) {
    return { valid: false, error: "Bad request: request body is invalid!" };
  }

  // check if origin and destination provided is of correct length
  if (origin.length !== 2 || destination.length !== 2) {
    return { valid: false, error: "Bad request: request body is invalid!" };
  }

  // check if origin provided is not empty and valid type
  if (
    isNaN(parseFloat(origin?.at(0)?.trim() || "")) ||
    isNaN(parseFloat(origin?.at(1)?.trim() || ""))
  ) {
    return {
      valid: false,
      error: "Bad request: provided origin is invalid!",
    };
  }

  // check if destination provided is not empty and valid type
  if (
    isNaN(parseFloat(destination?.at(0)?.trim() || "")) ||
    isNaN(parseFloat(destination?.at(1)?.trim() || ""))
  ) {
    return {
      valid: false,
      error: "Bad request: provided destination is invalid!",
    };
  }

  const result: OrderRoutePoints = {
    originLatitude: parseFloat(origin?.at(0)?.trim() || ""),
    originLongitude: parseFloat(origin?.at(1)?.trim() || ""),
    destinationLatitude: parseFloat(destination?.at(0)?.trim() || ""),
    destinationLongitude: parseFloat(destination?.at(1)?.trim() || ""),
  };

  // check if latitudes provided are in range
  if (
    !isLatitude(result.originLatitude) ||
    !isLatitude(result.destinationLatitude)
  ) {
    return {
      valid: false,
      error: "Bad request: provided latitude is invalid!",
    };
  }

  // check if longitudes provided are in range
  if (
    !isLongitude(result.originLongitude) ||
    !isLongitude(result.destinationLongitude)
  ) {
    return {
      valid: false,
      error: "Bad request: provided longitude is invalid!",
    };
  }

  return { valid: true, result };
};

export const isLatitude = (num: number) => isFinite(num) && Math.abs(num) <= 90;
export const isLongitude = (num: number) =>
  isFinite(num) && Math.abs(num) <= 180;
