export type OrderResponseType = {
  id: string;
  distance: number;
  status: "UNASSIGNED" | "TAKEN";
};

export type OrderRoutePoints = {
  originLatitude: number;
  originLongitude: number;
  destinationLatitude: number;
  destinationLongitude: number;
};
