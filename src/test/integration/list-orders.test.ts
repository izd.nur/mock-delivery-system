import { Application } from "express";
import request from "supertest";
import { createApp } from "../util/app";
import { OrderModel } from "../../entities/order-entity";
import { assertArrayContains, assertArrayNotContains } from "../util/util";

const createOrder = async (
  app: Application,
  body: { origin: string[]; destination: string[] }
) =>
  request(app)
    .post("/orders")
    .send(body)
    .then((response) => response.body);

const listOrdersResponse = async (app: Application, query: any) =>
  request(app)
    .get("/orders")
    .query(query)
    .then((response) => response);

describe("List Orders", () => {
  let app: Application;

  beforeAll(async () => {
    app = await createApp();
  });

  it("should successfully list all orders", async () => {
    const createdOrderIds: string[] = [];
    for (let i = 0; i < 10; i++) {
      await createOrder(app, {
        origin: ["22.28601", "114.149481"],
        destination: ["22.281086", "114.184179"],
      }).then((order) => createdOrderIds.push(order.id));
    }

    const { statusCode, body: listedOrders } = await listOrdersResponse(
      app,
      {}
    );
    expect(statusCode).toBe(200);
    expect(listedOrders.length).toBeGreaterThanOrEqual(10);
    assertArrayContains(
      listedOrders.map((order: OrderModel) => order.id),
      createdOrderIds
    );
  });

  it("should successfully list all orders with limit", async () => {
    for (let i = 0; i < 5; i++) {
      await createOrder(app, {
        origin: ["22.28601", "114.149481"],
        destination: ["22.281086", "114.184179"],
      });
    }

    const { statusCode, body: listedOrders } = await listOrdersResponse(app, {
      limit: 5,
    });
    expect(statusCode).toBe(200);
    expect(listedOrders.length).toBe(5);
  });

  it("should successfully list all orders with offset and limit", async () => {
    const createdOrderIds: string[] = [];
    for (let i = 0; i < 10; i++) {
      await createOrder(app, {
        origin: ["22.28601", "114.149481"],
        destination: ["22.281086", "114.184179"],
      }).then((order) => createdOrderIds.push(order.id));
    }

    const { statusCode, body: listedOrders } = await listOrdersResponse(
      app,
      {}
    );
    expect(statusCode).toBe(200);
    expect(listedOrders.length).toBeGreaterThanOrEqual(5);

    const { statusCode: offsetStatusCode, body: offsetListedOrders } =
      await listOrdersResponse(app, { limit: 3, page: 2 });
    expect(offsetStatusCode).toBe(200);
    assertArrayContains(
      offsetListedOrders.map((order: OrderModel) => order.id),
      listedOrders.map((order: OrderModel) => order.id).slice(3, 6)
    );
    assertArrayNotContains(
      offsetListedOrders.map((order: OrderModel) => order.id),
      listedOrders.map((order: OrderModel) => order.id).slice(0, 3)
    );
    assertArrayNotContains(
      offsetListedOrders.map((order: OrderModel) => order.id),
      listedOrders.map((order: OrderModel) => order.id).slice(6, 10)
    );
  });

  it("should fail to list orders (wrong page values)", async () => {
    let statusCode = await listOrdersResponse(app, {
      page: "asd",
    }).then((response) => response.statusCode);
    expect(statusCode).toBe(400);

    statusCode = await listOrdersResponse(app, {
      page: -1,
    }).then((response) => response.statusCode);
    expect(statusCode).toBe(400);

    statusCode = await listOrdersResponse(app, {
      page: 0,
    }).then((response) => response.statusCode);
    expect(statusCode).toBe(400);

    statusCode = await listOrdersResponse(app, {
      page: 1,
    }).then((response) => response.statusCode);
    expect(statusCode).toBe(200);
  });

  it("should fail to list orders (wrong limit values)", async () => {
    let statusCode = await listOrdersResponse(app, {
      limit: "asd",
    }).then((response) => response.statusCode);
    expect(statusCode).toBe(400);

    statusCode = await listOrdersResponse(app, {
      limit: -1,
    }).then((response) => response.statusCode);
    expect(statusCode).toBe(400);

    statusCode = await listOrdersResponse(app, {
      limit: 0,
    }).then((response) => response.statusCode);
    expect(statusCode).toBe(200);
  });

  it("should list empty array of orders (too large offset)", async () => {
    const { statusCode, body: listedOrders } = await listOrdersResponse(app, {
      page: "1000",
      limit: "10",
    }).then((response) => response);
    expect(statusCode).toBe(200);
    expect(listedOrders.length).toBe(0);
  });
});
