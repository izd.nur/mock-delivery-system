import { Application } from "express";
import request from "supertest";
import { createApp } from "../util/app";

const createOrderResponse = async (app: Application, body: any) =>
  request(app)
    .post("/orders")
    .send(body)
    .then((response) => response);

describe("Create Order", () => {
  let app: Application;

  beforeAll(async () => {
    app = await createApp();
  });

  it("should successfully create order", async () => {
    const body = {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086", "114.184179"],
    };
    const { statusCode, body: createdOrder } = await createOrderResponse(
      app,
      body
    );
    expect(statusCode).toBe(200);
    expect(createdOrder.id).toBeTruthy();
    expect(createdOrder.distance).toBeTruthy();
    expect(createdOrder.status).toBe("UNASSIGNED");
  });

  it("should fail to create order (no origin)", async () => {
    const body = {
      destination: ["22.281086", "114.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (no destination)", async () => {
    const body = {
      origin: ["22.28601", "114.149481"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (origin length < 2)", async () => {
    const body = {
      origin: ["22.28601"],
      destination: ["22.281086", "114.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (origin length > 2)", async () => {
    const body = {
      origin: ["22.28601", "114.149481", "114.149481", "114.149481"],
      destination: ["22.281086", "114.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (destination length < 2)", async () => {
    const body = {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (destination length > 2)", async () => {
    const body = {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086", "22.281086", "114.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (origin point is NaN)", async () => {
    const body = {
      origin: ["22.28601", "not-a-number"],
      destination: ["22.281086", "114.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (destination point is NaN)", async () => {
    const body = {
      origin: ["22.28601", "114.149481"],
      destination: ["not-a-number", "114.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (wrong latitude)", async () => {
    const body = {
      origin: ["92.28601", "114.149481"],
      destination: ["22.281086", "114.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });

  it("should fail to create order (wrong longitude)", async () => {
    const body = {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086", "184.184179"],
    };
    const { statusCode } = await createOrderResponse(app, body);
    expect(statusCode).toBe(400);
  });
});
