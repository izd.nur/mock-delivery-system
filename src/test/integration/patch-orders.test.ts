import { Application } from "express";
import request from "supertest";
import { createApp } from "../util/app";

const createOrder = async (
  app: Application,
  body: { origin: string[]; destination: string[] }
) =>
  request(app)
    .post("/orders")
    .send(body)
    .then((response) => response.body);

const patchOrderResponse = async (app: Application, id?: string, body?: any) =>
  request(app)
    .patch(`/orders/${id}`)
    .send(body)
    .then((response) => response);

describe("Patch Orders", () => {
  let app: Application;

  beforeAll(async () => {
    app = await createApp();
  });

  it("should successfully take order", async () => {
    const createdOrder = await createOrder(app, {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086", "114.184179"],
    });
    expect(createdOrder.status).toBe("UNASSIGNED");

    const { statusCode, body: patchedOrder } = await patchOrderResponse(
      app,
      createdOrder.id,
      { status: "TAKEN" }
    );
    expect(statusCode).toBe(200);
    expect(patchedOrder.status).toBe("SUCCESS");
  });

  it("should fail to successfully take order (no request body)", async () => {
    const createdOrder = await createOrder(app, {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086", "114.184179"],
    });
    expect(createdOrder.status).toBe("UNASSIGNED");

    const { statusCode } = await patchOrderResponse(app, createdOrder.id);
    expect(statusCode).toBe(400);
  });

  it("should fail to successfully take order (wrong status)", async () => {
    const createdOrder = await createOrder(app, {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086", "114.184179"],
    });
    expect(createdOrder.status).toBe("UNASSIGNED");

    const { statusCode } = await patchOrderResponse(app, createdOrder.id, {
      status: "CLAIMED",
    });
    expect(statusCode).toBe(400);
  });

  it("should fail to successfully take order (already taken)", async () => {
    const createdOrder = await createOrder(app, {
      origin: ["22.28601", "114.149481"],
      destination: ["22.281086", "114.184179"],
    });
    expect(createdOrder.status).toBe("UNASSIGNED");

    const { statusCode, body: patchedOrder } = await patchOrderResponse(
      app,
      createdOrder.id,
      { status: "TAKEN" }
    );
    expect(statusCode).toBe(200);
    expect(patchedOrder.status).toBe("SUCCESS");

    const { statusCode: conflictStatusCode } = await patchOrderResponse(
      app,
      createdOrder.id,
      { status: "TAKEN" }
    );
    expect(conflictStatusCode).toBe(409);
  });

  it("should fail to successfully take order (order does not exist - wrong id)", async () => {
    const { statusCode } = await patchOrderResponse(app, "wrong-id-123", {
      status: "TAKEN",
    });
    expect(statusCode).toBe(404);
  });

  it("should fail to successfully take order (order does not exist - no id provided)", async () => {
    const { statusCode } = await patchOrderResponse(app, undefined, {
      status: "TAKEN",
    });
    expect(statusCode).toBe(404);
  });
});
