import { getDistanceFromGoogleAPI } from "../../util/util";
import { OrderRoutePoints } from "../../util/types/types";

describe("Get distance from Google API", () => {
  it("should successfully calculate distance", async () => {
    const orderRoutes: OrderRoutePoints = {
      destinationLatitude: 10,
      destinationLongitude: 10,
      originLatitude: 30,
      originLongitude: 30,
    };
    const distance = await getDistanceFromGoogleAPI(orderRoutes);
    expect(distance).toBeGreaterThanOrEqual(6331000);
    expect(distance).toBeLessThanOrEqual(6336000);
  });

  it("should successfully calculate distance (HK points)", async () => {
    const orderRoutes: OrderRoutePoints = {
      destinationLatitude: 22.281086,
      destinationLongitude: 114.184179,
      originLatitude: 22.28601,
      originLongitude: 114.149481,
    };
    const distance = await getDistanceFromGoogleAPI(orderRoutes);
    expect(distance).toBeGreaterThanOrEqual(5700);
    expect(distance).toBeLessThanOrEqual(6000);
  });

  it("should fail to successfully calculate distance (wrong point - no results)", async () => {
    const orderRoutes: OrderRoutePoints = {
      destinationLatitude: 0,
      destinationLongitude: 5,
      originLatitude: 0,
      originLongitude: -3,
    };
    const distance = await getDistanceFromGoogleAPI(orderRoutes);
    expect(distance).toBe(-1);
  });
});
