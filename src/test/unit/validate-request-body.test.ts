import { checkRequestBodyIsValid } from "../../util/util";

describe("Get distance from Google API", () => {
  it("request body invalid (no origin)", async () => {
    const origin: string[] = [];
    const destination = ["22.28601", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (origin length = 1)", async () => {
    const origin = ["22.281086"];
    const destination = ["22.28601", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (origin length > 2)", async () => {
    const origin = ["22.281086", "114.184179", "114.184179"];
    const destination = ["22.28601", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (no destination)", async () => {
    const origin = ["22.281086", "114.184179"];
    const destination: string[] = [];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (destination length = 1)", async () => {
    const origin = ["22.281086", "114.184179"];
    const destination = ["22.28601"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (destination length > 2)", async () => {
    const origin = ["22.281086", "114.184179"];
    const destination = ["22.28601", "114.149481", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (origin empty string)", async () => {
    const origin = ["", "114.184179"];
    const destination = ["22.28601", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (destination empty string)", async () => {
    const origin = ["22.281086", "114.184179"];
    const destination = ["22.28601", "   "];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (origin is not a number)", async () => {
    const origin = ["xyzxy12", "114.184179"];
    const destination = ["22.28601", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (destination is not a number)", async () => {
    const origin = ["22.281086", "114.184179"];
    const destination = ["22.28601", "abgh128"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (not a latitude)", async () => {
    const origin = ["92.281086", "114.184179"];
    const destination = ["22.28601", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body invalid (not a longitude)", async () => {
    const origin = ["22.281086", "114.184179"];
    const destination = ["22.28601", "194.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(false);
    expect(error).toBeDefined();
    expect(result).toBeUndefined();
  });

  it("request body valid", async () => {
    const origin = ["22.281086", "114.184179"];
    const destination = ["22.28601", "114.149481"];
    const { valid, error, result } = checkRequestBodyIsValid(
      origin,
      destination
    );
    expect(valid).toBe(true);
    expect(error).toBeUndefined();
    expect(result).toBeDefined();
  });
});
