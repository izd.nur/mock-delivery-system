import express, { Application, Request, Response } from "express";
import { ConnectionOptions, createConnection } from "typeorm";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { OrderController } from "../../controllers/order-controller";
import { OrderModel } from "../../entities/order-entity";

export const createApp = async () => {
  const app: Application = express();

  app.set("port", process.env.PORT || 3000);
  app.use(express.json());

  const { DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, DB_PORT } = process.env;
  const connectionOptions: ConnectionOptions = {
    type: "postgres",
    host: DB_HOST || "localhost",
    port: 5433,
    username: DB_USER || "postgres",
    password: DB_PASSWORD || "password",
    database: DB_NAME || "postgres",
    entities: [OrderModel],
    name: "mock-delivery-db",
    namingStrategy: new SnakeNamingStrategy(),
    synchronize: true,
  };
  await createConnection(connectionOptions);

  const orderController = new OrderController();

  app.get("/", (req: Request, res: Response) => {
    res.send("Hello World!");
  });
  app.use("/orders", orderController.router);

  return app;
};
