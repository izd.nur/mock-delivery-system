export const assertArrayContains = (array: any[], subArray: any[]) => {
  expect(array).toStrictEqual(expect.arrayContaining(subArray));
};

export const assertArrayNotContains = (array: any[], subArray: any[]) => {
  subArray.forEach((item) =>
    expect(array).not.toBe(expect.arrayContaining([item]))
  );
};
