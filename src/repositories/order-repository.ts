import { EntityRepository, Repository } from "typeorm";
import { OrderModel } from "../entities/order-entity";

@EntityRepository(OrderModel)
export class OrderRepository extends Repository<OrderModel> {}
