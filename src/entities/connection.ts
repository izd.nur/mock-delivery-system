import { ConnectionOptions, createConnection } from "typeorm";
import { OrderModel } from "./order-entity";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";

export const createPostgresConnection = async (
  options: Partial<ConnectionOptions> = {}
) => {
  const {
    POSTGRES_HOST,
    POSTGRES_PORT,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    POSTGRES_DB,
  } = process.env;
  const connectionOptions: ConnectionOptions = {
    ...options,
    type: "postgres",
    host: POSTGRES_HOST || "localhost",
    port: POSTGRES_PORT ? parseInt(POSTGRES_PORT) : 5433,
    username: POSTGRES_USER || "admin",
    password: POSTGRES_PASSWORD || "password",
    database: POSTGRES_DB || "postgres",
    entities: [OrderModel],
    namingStrategy: new SnakeNamingStrategy(),
  };

  // TODO try to add wait-on later
  // await waitOn({
  //   resources: [`tcp:${connectionOptions.host}:${connectionOptions.port}`],
  //   timeout: 30_000,
  //   interval: 1_000,
  // });

  return await createConnection(connectionOptions);
};
