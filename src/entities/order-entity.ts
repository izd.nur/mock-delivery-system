import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity("orders")
export class OrderModel {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column("numeric", { nullable: false, default: 0 })
  distance!: number;

  @Column("text", { nullable: false, default: "UNASSIGNED" })
  status!: "UNASSIGNED" | "TAKEN";
}
