import { getConnection } from "typeorm";
import { OrderRoutePoints } from "../util/types/types";
import { OrderRepository } from "../repositories/order-repository";
import { OrderModel } from "../entities/order-entity";
import { getDistanceFromGoogleAPI } from "../util/util";

export class OrderService {
  private orderRepository: OrderRepository;

  constructor() {
    this.orderRepository =
      getConnection("mock-delivery-db").getCustomRepository(OrderRepository);
  }

  public createOrder = async (
    orderRoute: OrderRoutePoints
  ): Promise<OrderModel> => {
    const distance = await getDistanceFromGoogleAPI(orderRoute);

    const createdOrder: OrderModel = await this.orderRepository.save({
      distance: distance,
      status: "UNASSIGNED",
    });
    return createdOrder;
  };

  public getOrder = async (id?: string): Promise<OrderModel> => {
    return await this.orderRepository.findOneOrFail({
      id,
    });
  };

  public listOrders = async (
    page: number,
    limit: number
  ): Promise<OrderModel[]> => {
    return await this.orderRepository.find({
      take: limit,
      skip: (page - 1) * limit,
    });
  };

  public updateOrder = async (id: string): Promise<OrderModel> => {
    await this.orderRepository.update(
      {
        id,
      },
      {
        status: "TAKEN",
      }
    );
    return this.getOrder(id);
  };
}
