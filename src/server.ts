import express, { Application, Request, Response } from "express";
import { OrderController } from "./controllers/order-controller";
import { ConnectionOptions, createConnection } from "typeorm";
import { OrderModel } from "./entities/order-entity";
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import { config } from "dotenv";

config();

class Server {
  private orderController: OrderController;
  public app: Application;

  constructor() {
    this.app = express();
    this.configuration();
    this.routes();
  }

  public configuration() {
    this.app.set("port", process.env.PORT || 3000);
    this.app.use(express.json());
  }

  public async routes() {
    const { DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, DB_PORT } = process.env;
    const connectionOptions: ConnectionOptions = {
      type: "postgres",
      host: DB_HOST || "localhost",
      port: 5433,
      username: DB_USER || "postgres",
      password: DB_PASSWORD || "password",
      database: DB_NAME || "postgres",
      entities: [OrderModel],
      name: "mock-delivery-db",
      namingStrategy: new SnakeNamingStrategy(),
      synchronize: true,
    };
    await createConnection(connectionOptions);

    this.orderController = new OrderController();

    this.app.get("/", (req: Request, res: Response) => {
      res.send("Hello World!");
    });
    this.app.use("/orders", this.orderController.router);
  }

  public start() {
    this.app.listen(this.app.get("port"), () => {
      console.log(`Server is listening ${this.app.get("port")} port.`);
    });
  }
}

const server = new Server();
server.start();
