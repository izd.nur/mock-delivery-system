FROM node:16

WORKDIR /mock-delivery-system
COPY ./.env                             /mock-delivery-system/.env
COPY ./package.json                     /mock-delivery-system/package.json
COPY ./tsconfig.json                    /mock-delivery-system/tsconfig.json
COPY ./yarn.lock                        /mock-delivery-system/yarn.lock
COPY ./src                              /mock-delivery-system/src

RUN yarn install

COPY . .

WORKDIR /mock-delivery-system/src
RUN yarn build

EXPOSE 9000
ENV PORT 9000

WORKDIR /mock-delivery-system

RUN node ./build/server.js
